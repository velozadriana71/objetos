const userChosenKeyName = 'level';

const person = {
  'first name': "Adri",
  age: 23,
  hobbies: ["Sport", "Cooking"],
  //Buscara una variable con el ombre y va tomar el valor almacenado
  [userChosenKeyName]: 'Veloz',
  greet: function () {
    alert("Hi");
  },
  1.5: 'Hello'
};
//person.greet();

//Agregar un nuevo elemento al objeto
person.isAdmin = true;

//Eliminar alguna propiedad del objeto
delete person.age;
person.age = undefined; //No asignar a una propiedad el valor undefined.

const keyName = 'first name';

console.log(person[keyName]);
console.log(person[1.5]);

//Si se tiene un objeto con solo números no sé va mostrar como en el orden que se agrego en el
//Objeto se van a ordenar los números de menor a mayor