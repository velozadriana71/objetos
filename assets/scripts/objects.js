const addMovieBtn = document.getElementById("add-movie-btn");
const searchBtn = document.getElementById("search-btn");

const movies = [];

const renderMovies = (filter = "") => {
  const movieList = document.getElementById("movie-list");

  if (movies.length === 0) {
    movieList.classList.remove("visible");
    return;
  } else {
    movieList.classList.add("visible");
  }
  movieList.innerHTML = "";

  const filteredMovies = !filter
    ? movies
    : movies.filter((movie) => movie.info.title.includes(filter));

  filteredMovies.forEach((movie) => {
    const movieEl = document.createElement("li");
    //Se usa para saber si esta asignada, o si la propiedad no existe
    // if(!('info' in movie)){  } (movie.info === undefined)
    const { info, ...otherProps } = movie;
    console.log(otherProps);
    // const { title: titleMovie } = info;
    let { getFormattedTitle } = movie;
    //Bind prepara una función para una ejecución futura, retorna un nuevo objeto 
    // getFormattedTitle = getFormattedTitle.bind(movie);

    //CALL ejecuta de inmediato la función y puede pasar argumentos adicionales separado por comas
    let text = getFormattedTitle.call(movie) + " - ";

    //Apply también ejecuta la función de inmediato pero esta el primer argumento es lo
    // que se va ejecutar no tienen infinitos argumentos, en el segundo argumento toma una matriz
    //y esos pueden ser sus valores
    // let text = getFormattedTitle.apply(movie) + " - ";

    for (const key in info) {
      if (key !== "title") {
        text = text + `${key}: ${info[key]}`;
      }
    }
    movieEl.textContent = text;
    movieList.append(movieEl);
  });
};

const addMovieHandler = () => {
  const title = document.getElementById("title").value;
  const extraName = document.getElementById("extra-name").value;
  const extraValue = document.getElementById("extra-value").value;

  //Verificar si las cadenas que ingrese el usuario estan vacias.
  if (
    title.trim() === "" ||
    extraName.trim() === "" ||
    extraValue.trim() === ""
  ) {
    return;
  }
  const newMovie = {
    info: {
      title,
      [extraName]: extraValue,
    },
    id: Math.random().toString(),
    getFormattedTitle() {
      console.log(this);
      //This es a lo que esta mandando a llamar esa función
      return this.info.title.toUpperCase();
    }
  };
  //Agregar el objeto a la matriz que se creo globalmente
  movies.push(newMovie);
  renderMovies();
};

const searchMovieHandler = () => {
  //Cada función flecha tiene su propio this, las funciones flechas no vinculan a nada
  //mantienen el contexto que esto tiene con la vinculación que tendría fuera de la función
  console.log(this)
  const filterTerm = document.getElementById("filter-title").value;
  renderMovies(filterTerm);
};

addMovieBtn.addEventListener("click", addMovieHandler);
searchBtn.addEventListener("click", searchMovieHandler);
